-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ModeloRelacional
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ModeloRelacional` ;

-- -----------------------------------------------------
-- Schema ModeloRelacional
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ModeloRelacional` ;
USE `ModeloRelacional` ;

-- -----------------------------------------------------
-- Table `ModeloRelacional`.`Examen_ICFES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModeloRelacional`.`Examen_ICFES` ;

CREATE TABLE IF NOT EXISTS `ModeloRelacional`.`Examen_ICFES` (
  `idExamen_ICFES` INT NOT NULL AUTO_INCREMENT,
  `presentMunicipio` VARCHAR(45) NOT NULL,
  `presentDepartamento` VARCHAR(45) NOT NULL,
  `puntajeLecturaCritica` INT NOT NULL,
  `percentilLecturaCritica` INT NOT NULL,
  `desempLecturaCritica` INT NOT NULL,
  `puntajeMatematicas` INT NOT NULL,
  `percentilMatematicas` INT NOT NULL,
  `desempMatematicas` INT NOT NULL,
  `puntajeCNaturales` INT NOT NULL,
  `percentilCNaturales` INT NOT NULL,
  `desempCNaturales` INT NOT NULL,
  `puntajeSCiudadanas` INT NOT NULL COMMENT '		',
  `percentilSCiudadanas` INT NOT NULL,
  `desempSCiudadanas` INT NOT NULL,
  `puntajeIngles` INT NOT NULL,
  `percentilIngles` INT NOT NULL,
  `desempIngles` INT NOT NULL,
  `puntajeGlobal` INT NOT NULL,
  `percentilGlobal` INT NOT NULL,
  PRIMARY KEY (`idExamen_ICFES`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModeloRelacional`.`Estudiante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModeloRelacional`.`Estudiante` ;

CREATE TABLE IF NOT EXISTS `ModeloRelacional`.`Estudiante` (
  `idEstudiante` INT NOT NULL AUTO_INCREMENT,
  `genero` VARCHAR(45) NOT NULL,
  `periodo` INT NOT NULL,
  `consecutivo` VARCHAR(45) NOT NULL,
  `tieneEtnia` VARCHAR(45) NOT NULL,
  `paisResidencia` VARCHAR(45) NOT NULL,
  `etnia` VARCHAR(45) NULL,
  `deptoResidencia` VARCHAR(45) NULL,
  `mcpioResidencia` VARCHAR(45) NULL,
  `dedicacionLecturaDiaria` VARCHAR(45) NULL,
  `dedicacionInternet` VARCHAR(45) NULL,
  `horasSemanaTrabaja` VARCHAR(45) NULL COMMENT 'Número de horas que trabaja el estudiante en la semana',
  `tipoRemuneracion` VARCHAR(45) NULL,
  `fk_examen` INT NOT NULL,
  PRIMARY KEY (`idEstudiante`),
  INDEX `fk_Estudiante_Examen_ICFES1_idx` (`fk_examen` ASC) VISIBLE,
  CONSTRAINT `fk_examen`
    FOREIGN KEY (`fk_examen`)
    REFERENCES `ModeloRelacional`.`Examen_ICFES` (`idExamen_ICFES`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModeloRelacional`.`Familia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModeloRelacional`.`Familia` ;

CREATE TABLE IF NOT EXISTS `ModeloRelacional`.`Familia` (
  `idFamilia` INT NOT NULL AUTO_INCREMENT,
  `estrato` VARCHAR(45) NULL,
  `personasHogar` VARCHAR(45) NULL,
  `cuartosHogar` VARCHAR(45) NULL COMMENT 'cuartosHogar = Número de cuartos en el hogar',
  `eduPadre` VARCHAR(100) NULL,
  `eduMadre` VARCHAR(100) NULL,
  `laborPadre` VARCHAR(200) NULL,
  `laborMadre` VARCHAR(200) NULL COMMENT 'Trabajo o labor de la madre',
  `tieneInternet` VARCHAR(45) NULL,
  `tieneTV` VARCHAR(45) NULL,
  `tieneComputador` VARCHAR(45) NULL,
  `tieneLavadora` VARCHAR(45) NULL,
  `tieneMicroondas` VARCHAR(45) NULL,
  `tieneCarro` VARCHAR(45) NULL,
  `tieneMoto` VARCHAR(45) NULL,
  `tieneConsolaVideojuegos` VARCHAR(45) NULL,
  `numLibros` VARCHAR(45) NULL COMMENT 'Número de libros que leen en la familia',
  `consumenDerivadosLeche` VARCHAR(45) NULL COMMENT 'Cuantas veces por semana consumen derivados de leche.',
  `consumenCarnes` VARCHAR(45) NULL COMMENT 'Consumen carne, pollo, pescado',
  `consumenCerealesFrutasLegumbres` VARCHAR(45) NULL,
  `situacionEconomica` VARCHAR(45) NULL,
  `fk_estudiante_familia` INT NOT NULL,
  PRIMARY KEY (`idFamilia`),
  INDEX `fk_Familia_Estudiante_idx` (`fk_estudiante_familia` ASC) VISIBLE,
  CONSTRAINT `fk_estudiante_familia`
    FOREIGN KEY (`fk_estudiante_familia`)
    REFERENCES `ModeloRelacional`.`Estudiante` (`idEstudiante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ModeloRelacional`.`Colegio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ModeloRelacional`.`Colegio` ;

CREATE TABLE IF NOT EXISTS `ModeloRelacional`.`Colegio` (
  `idColegio` INT NOT NULL AUTO_INCREMENT,
  `nombreColegio` VARCHAR(45) NOT NULL,
  `genero` VARCHAR(45) NOT NULL,
  `naturaleza` VARCHAR(45) NOT NULL,
  `calendario` VARCHAR(45) NOT NULL,
  `bilingue` VARCHAR(45) NULL,
  `caracter` VARCHAR(45) NULL,
  `nombreSede` VARCHAR(45) NOT NULL,
  `sedePrincipal` VARCHAR(45) NOT NULL,
  `areaUbicacion` VARCHAR(45) NOT NULL,
  `jornada` VARCHAR(45) NOT NULL,
  `municipio` VARCHAR(45) NOT NULL,
  `departamento` VARCHAR(45) NOT NULL,
  `fk_estudiante_colegio` INT NOT NULL,
  PRIMARY KEY (`idColegio`),
  INDEX `fk_Colegio_Estudiante1_idx` (`fk_estudiante_colegio` ASC) VISIBLE,
  CONSTRAINT `fk_estudiante_colegio`
    FOREIGN KEY (`fk_estudiante_colegio`)
    REFERENCES `ModeloRelacional`.`Estudiante` (`idEstudiante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
